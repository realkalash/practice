package com.example.practice;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class ApiService {
    private static final String API = "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/";
    private static PrivateApi privateApi;
    private static String apiKey = "DEMO_KEY";

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);

    }


    public static Observable<ResponsePhotoModel> getAllPhotos(String sol, String apiKey) {
        return privateApi.getPhotos(sol, apiKey);
    }

    public interface PrivateApi {

        @GET("photos")
        Observable<ResponsePhotoModel> getPhotos(@Query("sol") String sol, @Query("api_key") String apiKey);
    }

}
