package com.example.practice;

import java.util.ArrayList;
import java.util.List;

public class Convertor {
    public static List<PhotoModel> convertRequest(ResponsePhotoModel responsePhotoModels) {

        List<PhotoModel> photos = new ArrayList<>();
        for (int i = 0; i < responsePhotoModels.getPhotos().size(); i++) {
            photos.add(new PhotoModel(i+1, responsePhotoModels.getPhotos().get(i).getImg_src()));

        }
        return photos;
    }
}
