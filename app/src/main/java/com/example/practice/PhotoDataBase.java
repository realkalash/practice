package com.example.practice;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {PhotoModel.class}, version = 12)
public abstract class PhotoDataBase extends RoomDatabase {

    public abstract PhotoDao getPhotosDao();

}
