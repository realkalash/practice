package com.example.practice;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.imageView)
    ImageView imageView;

    List<PhotoModel> photoModelsList;
    int position = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        final PhotoDataBase photoDataBase = Room
                .databaseBuilder(this, PhotoDataBase.class, "database")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build();


        if (photoDataBase.getPhotosDao().countAll() > 0) {
            Disposable disposable = photoDataBase.getPhotosDao().selectAll()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<List<PhotoModel>>() {
                        @Override
                        public void accept(List<PhotoModel> photoModels) throws Exception {
                            Random random = new Random();
                            int randomImage = random.nextInt(photoModels.size());

                            photoModelsList = photoModels;

                            touchListener();

                            setPhoto(photoModels, position);
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Toast.makeText(MainActivity.this, "Error with DataBase", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            Disposable disposable = ApiService.getAllPhotos("1000", "NNKOjkoul8n1CH18TWA9gwngW1s1SmjESPjNoUFo")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<ResponsePhotoModel>() {
                        @Override
                        public void accept(ResponsePhotoModel responsePhotoModels) throws Exception {
                            Random random = new Random();

                            int randomPhoto = random.nextInt(responsePhotoModels.getPhotos().size());

                            setPhoto(photoModelsList, position);
                            touchListener();
                            photoModelsList = Convertor.convertRequest(responsePhotoModels);
                            photoDataBase.getPhotosDao().insertAll(photoModelsList);


                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Toast.makeText(MainActivity.this, "Error with internet", Toast.LENGTH_SHORT).show();
                            throwable.printStackTrace();
                        }
                    });
        }

    }

    private void setPhoto(List<PhotoModel> photoModels, int position) {
        Picasso.with(MainActivity.this)
                .load(photoModelsList.get(position).getSrc())
                .into(imageView);
    }

    public void touchListener() {
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;
                int height = size.y;

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    //left button
                    if (event.getRawX() < width / 2 && event.getRawY() > height / 2) {
                        if (position > 0) {
                            Toast.makeText(MainActivity.this, "Prev", Toast.LENGTH_SHORT).show();
                            position--;
                            setPhoto(photoModelsList, position);
                        } else
                            Toast.makeText(MainActivity.this, "End list", Toast.LENGTH_SHORT).show();
                    }

                    //right button
                    if (event.getRawX() >= width / 2 && event.getRawY() > height / 2) {
                        Toast.makeText(MainActivity.this, "Next", Toast.LENGTH_SHORT).show();
                        if (position < photoModelsList.size()) {
                            position++;
                            setPhoto(photoModelsList, position);
                        } else
                            Toast.makeText(MainActivity.this, "End list", Toast.LENGTH_SHORT).show();
                    }
                }


                return true;
            }
        });

    }
}
