package com.example.practice;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class PhotoDao {

    @Insert
    public abstract void insertAll(List<PhotoModel> photoModels);

    @Query("SELECT * FROM PhotoModel")
    // позволяет использовать буферизацию. Когда выдираем большое кол-во данных, можно выдирать только по чуть-чуть к примеру обьектов
    public abstract Flowable<List<PhotoModel>> selectAll();                     // тот же обзёрв


    @Query("SELECT COUNT(*) FROM PhotoModel")
    // позволяет использовать буферизацию. Когда выдираем большое кол-во данных, можно выдирать только по чуть-чуть к примеру обьектов
    public abstract Integer countAll();

    @Query("DELETE FROM PhotoModel")
    public abstract void removeAll();

}
